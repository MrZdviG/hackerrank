import numpy
#
n, m = map(int, input().split())
numpy.set_printoptions(sign=' ')

A = numpy.array([input().split() for i in range(n)], int)
B = numpy.array([input().split() for i in range(n)], int)

print(A + B)
print(A - B)
print(A * B)
print(A // B)
print(A % B)
print(A ** B)
