import numpy

n, m = map(int, input().split())
arr = [list(map(int, input().split())) for i in range(n)]

print(numpy.transpose(arr))
print(numpy.array(arr).flatten())
