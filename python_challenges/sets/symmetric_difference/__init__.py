eng_n = int(raw_input())
eng_set = set(map(int, raw_input().split()))

fr_n = int(raw_input())
fr_set = set(map(int, raw_input().split()))

for i in sorted(eng_set.symmetric_difference(fr_set)):
    print i
