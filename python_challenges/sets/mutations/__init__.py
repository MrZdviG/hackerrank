set1_n = int(raw_input())
set1 = set(map(int, raw_input().split()))

for _ in range(int(raw_input())):
    command, set_size = raw_input().split()
    tmp_set = set(map(int, raw_input().split()))
    if command == "intersection_update":
        set1.intersection_update(tmp_set)
    elif command == "update":
        set1.update(tmp_set)
    elif command == "symmetric_difference_update":
        set1.symmetric_difference_update(tmp_set)
    elif command == "difference_update":
        set1.difference_update(tmp_set)

print sum(set1)
