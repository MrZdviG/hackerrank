n = int(raw_input())
set1 = set()
set2 = set()
for i in map(int, raw_input().split()):
    if i not in set1:
        set1.update([i])
    else:
        set2.update([i])

print set1.difference(set2).pop()
