# pop
# remove 9
# discard 9
# discard 8
# remove 7
# pop
# discard 6
# remove 5
# pop
# discard 5

n = int(raw_input())
set1 = set(map(int, raw_input().split()))
for _ in range(int(raw_input())):
    tmp = raw_input().strip().split()
    if len(tmp) > 1:
        command, data = tmp[0].strip(), int(tmp[1])
    else:
        command = tmp[0]
    if command == "pop":
        set1.pop()
    elif command == "remove":
        set1.remove(data)
    elif command == "discard":
        set1.discard(data)

print sum(set1)