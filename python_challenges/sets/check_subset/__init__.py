test_amount = int(raw_input())

for _ in range(test_amount):
    tmp_n1 = int(raw_input())
    tmp_set1 = set(map(int, raw_input().split()))

    tmp_n2 = int(raw_input())
    tmp_set2 = set(map(int, raw_input().split()))

    print "True" if len(tmp_set1.difference(tmp_set2)) == 0 else "False"