strict_superset = set(map(int, raw_input().split()))
tmp_ar = []
for _ in range(int(raw_input())):
    tmp_set = set(map(int, raw_input().split()))

    if len(strict_superset) > len(tmp_set) and len(tmp_set.difference(strict_superset)) == 0:
        tmp_ar.append(True)
    else:
        tmp_ar.append(False)

print "False" if False in tmp_ar else "True"