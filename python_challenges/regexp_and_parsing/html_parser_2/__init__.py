from HTMLParser import HTMLParser

html = ""
for _ in range(int(raw_input())):
    html += raw_input()
    html += '\n'


class MyHTMLParser(HTMLParser):
    def handle_comment(self, data):
        if '\n' in data:
            print ">>> Multi-line Comment"
            for i in data.split('\n'):
                print i
        else:
            print ">>> Single-line Comment"
            print data

    def handle_data(self, data):
        if len(data) > 1:
            print ">>> Data"
            print data


parser = MyHTMLParser()
parser.feed(html)
