from HTMLParser import HTMLParser

html = ""
for _ in range(int(raw_input())):
    html += raw_input()
    html += '\n'


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        print tag
        if len(attrs) > 0:
            for i in attrs:
                print "->", i[0], ">", i[1]


parser = MyHTMLParser()
parser.feed(html)
