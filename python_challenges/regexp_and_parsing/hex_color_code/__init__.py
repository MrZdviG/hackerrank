import re

for _ in range(int(raw_input())):
    result = re.findall(r'(#[A-Fa-f0-9]{3,6})\s*[;,\)]', raw_input())
    if len(result) > 0:
        for i in result:
            print(i)
