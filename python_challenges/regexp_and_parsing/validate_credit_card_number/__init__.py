import re


def validate(s):
    if re.match(r'^([456]\d{3}-?)(\d{4}-?){2}(\d{4})$', s) and \
            len(re.findall(r'(\d)(\1){3}', re.sub(r'-', '', s))) == 0:
        return True
    return False


for _ in range(int(raw_input())):
    s = raw_input()
    print "Valid" if validate(s) else "Invalid"
