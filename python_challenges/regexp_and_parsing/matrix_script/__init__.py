import re


first_multiple_input = raw_input().rstrip().split()

n = int(first_multiple_input[0])

m = int(first_multiple_input[1])

matrix = ["" for k in range(m)]

for _ in xrange(n):
    matrix_item = raw_input()
    for i in range(m):
        matrix[i] += matrix_item[i]

print re.sub(r"(?<=\w)([\$#%\s]+)(?=\w)", " ", "".join(matrix))
