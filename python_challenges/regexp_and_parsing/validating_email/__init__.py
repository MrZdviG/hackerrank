import re
import email.utils

for _ in range(int(raw_input())):
    addr = raw_input().strip()
    if re.match(r"^[A-Za-z][\d\w_\-.]*@[A-Za-z]+\.[A-Za-z]{1,3}$", email.utils.parseaddr(addr)[1]):
        print addr
