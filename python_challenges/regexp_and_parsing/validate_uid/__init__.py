import re


def validate(s):
    if re.match(r'[A-Za-z0-9]', s) and \
            len(re.findall(r'[A-Z]', s)) >= 2 and \
            len(re.findall(r'[0-9]', s)) >= 3 and \
            len(s.strip()) == 10 and \
            len(set(s)) == 10:
        return True
    return False


for _ in range(int(raw_input())):
    s = raw_input()
    print "Valid" if validate(s) else "Invalid"
