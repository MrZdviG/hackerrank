for _ in range(int(raw_input())):
    try:
        tmp1, tmp2 = map(int, raw_input().split())
        print(tmp1 / tmp2)
    except BaseException as e:
        print "Error Code:", e
