from __future__ import print_function


# Fizz Buzz program
# Every element that divides without a reminder to:
# 3 swaps to Fizz
# 5 swaps to Buzz
# 3 and 5 at the same time swaps to Fizz Buzz

def fizzbuzz(size):
    for i in range(1, size + 1):
        if i % 3 == 0 and i % 5 == 0:
            print("Fizz Buzz")
        elif i % 3 == 0:
            print("Fizz")
        elif i % 5 == 0:
            print("Buzz")
        else:
            print(i)


if __name__ == "__main__":
    SIZE = 100
    fizzbuzz(SIZE)
