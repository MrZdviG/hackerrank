from __future__ import print_function
from itertools import groupby

s = groupby(raw_input())

for k, g in s:
    print("({}, {})".format(len(list(g)), k), end=" ")
