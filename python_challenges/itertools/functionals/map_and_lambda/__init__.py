cube = lambda x: x ** 3

def fibonacci(n):
    result = []
    if n < 2:
        for i in range(0,n):
            result.append(i)
    else:
        a = 0
        b = 1
        result = [a, b]
        n = n - 2
        for _ in range(n):
            a, b = b, a+b
            result.append(b)
    return result


