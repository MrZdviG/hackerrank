from __future__ import print_function
from itertools import product

a = map(int, raw_input().split())
b = map(int, raw_input().split())

for i in list(product(a, b)):
    print(i, end=" ")
