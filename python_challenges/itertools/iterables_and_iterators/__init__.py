from itertools import combinations

n = int(raw_input())
s = raw_input()
k = int(raw_input())
comb = combinations(s.split(), k)
count = tmp = 0
for i in comb:
    count += 1
    if 'a' in i:
        tmp += 1

print ("{0:.4f}".format(tmp/float(count)))
