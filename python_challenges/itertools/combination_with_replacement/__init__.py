from __future__ import print_function
from itertools import combinations, combinations_with_replacement

input_ar = raw_input().split()

tmp = list(combinations_with_replacement(sorted(input_ar[0]), int(input_ar[1])))
for y in tmp:
    for g in y:
        print(g, end="")
    print("")
