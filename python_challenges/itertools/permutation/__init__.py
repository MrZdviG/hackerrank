from __future__ import print_function
from itertools import permutations

input_string = raw_input().split()

for i in permutations(sorted(input_string[0]), int(input_string[1])):
    for y in range(int(input_string[1])):
        print(i[y], end="")
    print("")
