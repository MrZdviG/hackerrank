from itertools import product

k, m = map(int, raw_input().split())
arr = []
for _ in range(k):
    arr.append(map(int, raw_input().split()[1:]))

print(max([sum((j ** 2) for j in i) % m for i in product(*arr)]))
