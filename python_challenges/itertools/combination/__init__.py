from __future__ import print_function
from itertools import combinations

input_ar = raw_input().split()

for i in range(1, int(input_ar[1])+1):
    tmp = list(combinations(sorted(input_ar[0]), i))
    for y in tmp:
        for g in y:
            print(g, end="")
        print("")
