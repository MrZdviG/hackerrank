from __future__ import print_function
import calendar

d = map(int, raw_input().split())
print((calendar.day_name[calendar.weekday(d[2], d[0], d[1])]).upper())
