#!/bin/python

import os
import datetime
import calendar


# Complete the time_delta function below.
def time_delta(t1, t2):
    months = list(calendar.month_abbr)
    ar = []
    for _ in t1, t2:
        tmp = _.split()
        gtime = tmp[4].split(":")
        offset = tmp[5]
        offset_time = datetime.timedelta(0,
                                         0,
                                         0,
                                         0,
                                         float(offset[3:5]),
                                         float(offset[1:3]))
        orig_date = datetime.datetime(int(tmp[3]),
                                      months.index(tmp[2]),
                                      int(tmp[1]),
                                      int(gtime[0]),
                                      int(gtime[1]),
                                      int(gtime[2]),
                                      0)
        if offset[0] == "+":
            orig_date -= offset_time
        else:
            orig_date += offset_time
        ar.append(orig_date)

    return str(abs(int((ar[0] - ar[1]).total_seconds())))


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(raw_input())

    for t_itr in xrange(t):
        t1 = raw_input()

        t2 = raw_input()

        delta = time_delta(t1, t2)

        fptr.write(delta + '\n')

    fptr.close()
