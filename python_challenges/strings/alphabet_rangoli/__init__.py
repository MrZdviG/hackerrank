
import string


def print_rangoli(size):
    alpha = string.ascii_lowercase
    # upper part
    for i in reversed(range(size)):
        s = "-".join(alpha[i:size])
        print (s[::-1]+s[1:]).center(4*size-3, "-")

    # lower part without first line
    for i in range(1,size):
        s = "-".join(alpha[i:size])
        print (s[::-1]+s[1:]).center(4*size-3, "-")

