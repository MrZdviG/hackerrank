from __future__ import print_function
import math


a = int(raw_input().strip())
b = int(raw_input().strip())
c = math.sqrt(a*a+b*b)
angle = math.acos(b/c)

print(("{:0.0f}".format(round(math.degrees(angle)))) + u"\u00b0")
