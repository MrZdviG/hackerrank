from collections import defaultdict

if __name__ == "__main__":
    n, m = (map(int, raw_input().split()))
    result = defaultdict(list)
    a = []
    b = []

    for _ in range(n):
        a.append(raw_input())

    for _ in range(m):
        b.append(raw_input())

    for i in range(len(a)):
        result[a[i]].append(i + 1)

    for i in b:
        if len(result[i]) > 0:
            print(' '.join(map(str, result[i])))
        else:
            print -1
