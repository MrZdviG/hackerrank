from collections import deque


def check_vertical_piling(count, case):
    tmp = case[0] if case[0] >= case[-1] else case[-1]
    for _ in range(count):
        if case[-1] <= case[0] <= tmp:
            case.popleft()
        elif case[0] <= case[-1] <= tmp:
            case.pop()
        else:
            return "No"
    return "Yes"


if __name__ == "__main__":
    test_cases = []
    for i in range(int(raw_input())):
        count = int(raw_input())
        test_cases.append([count, deque(map(int, raw_input().split()))])

    for test_case in test_cases:
        print check_vertical_piling(test_case[0], test_case[1])
