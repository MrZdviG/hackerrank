from __future__ import print_function
from collections import OrderedDict
n = int(raw_input())
od = OrderedDict()
for _ in range(n):
    tmp = raw_input()
    if tmp in od:
        od[tmp] += 1
    else:
        od[tmp] = 1

print(len(od))
for k,v in od.items():
    print(v, end=" ")
