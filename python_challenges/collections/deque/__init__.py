from __future__ import print_function
from collections import deque

queue = deque()

for _ in range(int(raw_input())):
    tmp = raw_input().split()
    if tmp[0] == 'append':
        queue.append(tmp[1])
    elif tmp[0] == 'appendleft':
        queue.appendleft(tmp[1])
    elif tmp[0] == 'pop':
        queue.pop()
    elif tmp[0] == 'popleft':
        queue.popleft()
    else:
        print("This functione isn't realized yet")

for i in queue:
    print(i, end=" ")
