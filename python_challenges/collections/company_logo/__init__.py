from collections import Counter


if __name__ == '__main__':
    NUMBER_SYMBOLS = 3
    occur_list = Counter(raw_input()).items()
    for k, v in sorted(occur_list, key=lambda c: (-c[1], c[0]))[:NUMBER_SYMBOLS]:
        print("{} {}".format(k, v))
