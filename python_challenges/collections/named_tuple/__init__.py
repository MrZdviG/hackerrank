from collections import namedtuple
n, Student = int(raw_input()), namedtuple('Student', raw_input())
grades = [raw_input().split() for _ in range(n)]
print("{0:.2f}".format(sum(map(lambda x: float(x.MARKS), [Student(i[0], i[1], i[2], i[3]) for i in grades]))/n))

#
# n, marks_index = int(raw_input()), raw_input().split().index("MARKS")
# tmp = 0
# for _ in range(n): tmp += int(raw_input().split()[marks_index])
# print "{0:.2f}".format(float(tmp)/n)
