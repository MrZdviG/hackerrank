from collections import OrderedDict
n, sold_items = int(raw_input()), OrderedDict()
for _ in range(n):
    tmp = raw_input().split()
    name = " ".join(tmp[0:len(tmp)-1])
    quantity = int(tmp[-1])
    if name in sold_items:
        sold_items[name] += quantity
    else:
        sold_items[name] = quantity
for k, v in sold_items.items():
    print k, v
