from collections import Counter

if __name__ == "__main__":
    x = int(raw_input())
    ar = map(int, list(raw_input().split()))
    n = int(raw_input())
    customers = []
    for _ in range(n):
        customers.append(map(int, list(raw_input().split())))

    shoes = Counter(ar)
    cash = 0

    for i in customers:
        if i[0] in shoes and shoes[i[0]] > 0:
            cash += i[1]
            shoes.subtract([i[0]])

    print cash
